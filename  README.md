 générateur de memes:
 
Mettre au point un générateur de meme sur le principe de https://imgflip.com/memegenerator (définition du meme : http://knowyourmeme.com/).

A partir d’un support image, l’utilisateur devra pouvoir ajouter un contenu texte personnalisé pour le transformer en même.

L’image générée devra être sauvegardée sur une adresse url accessible.

Le résultat devra être partageable sur les réseaux sociaux (facebook et twitter ...).

 

Bonus 1: Une page d’accueil liste les derniers memes créés.

Bonus 2: les pages générées sont indexables par les moteurs de recherche.

Bonus Ninja: utiliser http://api.giphy.com/ pour le choix des images.

Projet de groupe :

    Otmane EZ ZIANI
    Pierre HERMEY
    Arnaud WAWRZYNIAK
    Raphaël CARREY

Pour isiter le site : http://otmanee.promo-19.codeur.online/meme-maker